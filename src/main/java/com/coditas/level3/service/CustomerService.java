package com.coditas.level3.service;

import com.coditas.level3.entity.Customer;
import com.coditas.level3.entity.Restaurant;
import com.coditas.level3.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerService {


    @Autowired
    CustomerRepository customerRepository;

    public String addCustomer(Customer customer)
    {
        if(customerRepository.findAll().stream().filter(c->c.getCustomerName().equals(customer.getCustomerName())).collect(Collectors.toList()).size()==0) {
            Customer customer1 = new Customer();

            customer1.setCustomerName(customer.getCustomerName());
            customer1.setX(customer.getX());
            customer1.setY(customer.getY());

            customerRepository.save(customer1);
            return "Customer added sucesfully";
        }
        else
        {
            return "Customer already exists";
        }
    }

}
