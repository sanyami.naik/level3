package com.coditas.level3.service;


import com.coditas.level3.dto.OrderTableDto;
import com.coditas.level3.entity.Customer;
import com.coditas.level3.entity.FoodItem;
import com.coditas.level3.entity.OrderTable;
import com.coditas.level3.entity.Restaurant;
import com.coditas.level3.repository.CustomerRepository;
import com.coditas.level3.repository.FoodItemRepository;
import com.coditas.level3.repository.OrderRepository;
import com.coditas.level3.repository.RestaurantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    FoodItemRepository foodItemRepository;

    @Autowired
    RestaurantRepository restaurantRepository;


    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    OrderRepository orderRepository;

    public String addOrder(OrderTableDto orderTableDto)
    {
        OrderTable orderTable=new OrderTable();

        FoodItem foodItem=foodItemRepository.findById(orderTableDto.getFoodItemId()).get();

        Restaurant restaurant=restaurantRepository.findById(orderTableDto.getRestaurantId()).get();


        Customer customer=customerRepository.findById(orderTableDto.getCustomerId()).get();

        orderTable.setCustomer(customer);
        orderTable.setRestaurant((restaurant));
        orderTable.setFoodItem(foodItem);
        orderTable.setDate(LocalDate.now());

        orderRepository.save(orderTable);
        return "order placed wait for some time";

    }
}
