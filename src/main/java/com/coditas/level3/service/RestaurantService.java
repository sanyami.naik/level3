package com.coditas.level3.service;


import com.coditas.level3.dto.RestaurantDto;
import com.coditas.level3.dto.UpdateOrderDto;
import com.coditas.level3.entity.*;
import com.coditas.level3.repository.DeliveryRepository;
import com.coditas.level3.repository.OrderRepository;
import com.coditas.level3.repository.RestaurantRepository;
import org.apache.tomcat.jni.Time;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RestaurantService {

    @Autowired
    RestaurantRepository restaurantRepository;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    DeliveryRepository deliveryRepository;

    public String addRestaurant(Restaurant restaurant)
    {

        if(restaurantRepository.findAll().stream().filter(r->r.getRestaurantName().equals(restaurant.getRestaurantName())).collect(Collectors.toList()).size()==0) {
            Restaurant restaurant1 = new Restaurant();
            restaurant1.setRestaurantName(restaurant.getRestaurantName());
            restaurant1.setRating(restaurant.getRating());
            restaurant1.setX(restaurant.getX());
            restaurant1.setY(restaurant.getY());

            restaurantRepository.save(restaurant1);

            return "Restaurant added succesfully";
        }
        else {
            return "Restaurant with that name already exists";
        }
    }


    public List<RestaurantDto> restaurantRating()
    {
        List<Restaurant> restaurantList=restaurantRepository.findAll();
        System.out.println(restaurantList);
        return restaurantList.stream().sorted(Comparator.comparing(Restaurant::getRating).reversed()).map(r->mapToDto(r)).collect(Collectors.toList());
    }

    public String updateOrder(UpdateOrderDto updateOrderDto) {
        OrderTable orderTable=orderRepository.findById(updateOrderDto.getOrderId()).get();

        System.out.println(updateOrderDto.getOrderStatus());
        if(updateOrderDto.getOrderStatus().equals(String.valueOf(Status.PREPARED)))
        {
            orderTable.setOrderStatus(String.valueOf(Status.PREPARED));
            orderRepository.save(orderTable);
            return "Order has been prepared.. Wait for dispatcched";
        }
        else if(updateOrderDto.getOrderStatus().equals(String.valueOf(Status.DISPATCHED))) {

            Customer customer=orderTable.getCustomer();
            int xOfCustomer=customer.getX();
            int yOfCustomer=customer.getY();

            DeliveryMan deliveryMan=deliveryRepository.findById(updateOrderDto.getDeliveryBoyId()).get();
            int xOfDeliveryMan=deliveryMan.getX();
            int yOfDeliveryMan=deliveryMan.getY();

            Restaurant restaurant=orderTable.getRestaurant();
            int xOfRestaurant=restaurant.getX();
            int yOfRestaurant=restaurant.getY();

            double distance1=Math.sqrt(Math.pow(xOfDeliveryMan-xOfRestaurant,2)+Math.pow(yOfDeliveryMan-yOfRestaurant,2));
            double distance2=Math.sqrt(Math.pow(xOfRestaurant-xOfCustomer,2)+Math.pow(yOfRestaurant-yOfCustomer,2));

            double totalDistance=distance1+distance2;
            System.out.println(totalDistance);

            double time= Math.ceil(totalDistance);

            orderTable.setTime((int) time);
            orderTable.setDeliveryMan(deliveryMan);
            orderTable.setDispatchTime(new Date().getTime());
            orderTable.setOrderStatus(String.valueOf(Status.DISPATCHED));


            List<OrderTable> orderTableList=deliveryMan.getOrderList();
            long count =orderTableList.stream().filter(orderTable1 -> orderTable1.getOrderStatus().equals(String.valueOf(Status.DELIVERED))).collect(Collectors.toList()).size();
            if(count==orderTableList.size())
            {
                orderRepository.save(orderTable);
                return "Order has been dispatched";
            }

            else {
                return "Find another deliveryMan ";
            }

        }

       return null;


    }


    public RestaurantDto mapToDto(Restaurant restaurant)
    {
        RestaurantDto restaurantDto=new RestaurantDto();
        restaurantDto.setRating(restaurant.getRating());
        restaurantDto.setRestaurantName(restaurant.getRestaurantName());
        restaurantDto.setX(restaurant.getX());
        restaurantDto.setY(restaurant.getY());
        return restaurantDto;
    }
}
