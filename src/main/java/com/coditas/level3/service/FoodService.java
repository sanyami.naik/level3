package com.coditas.level3.service;


import com.coditas.level3.dto.FoodItemDto;
import com.coditas.level3.entity.FoodItem;
import com.coditas.level3.entity.Restaurant;
import com.coditas.level3.repository.FoodItemRepository;
import com.coditas.level3.repository.RestaurantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FoodService {

    @Autowired
    FoodItemRepository foodItemRepository;

    @Autowired
    RestaurantRepository restaurantRepository;

    public String addFoodItem(FoodItemDto foodItemDto)
    {
        FoodItem foodItem=new FoodItem();
        foodItem.setFoodName(foodItemDto.getFoodName());
        foodItem.setFoodPrice(foodItemDto.getFoodPrice());

        Restaurant restaurant=restaurantRepository.findById(foodItemDto.getRestaurantId()).get();
        foodItem.setRestaurant(restaurant);

        foodItemRepository.save(foodItem);

        return "FoodItem added !!";

    }
}
