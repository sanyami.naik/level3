package com.coditas.level3.service;
import com.coditas.level3.dto.DeliveryManDto;
import com.coditas.level3.dto.UpdateOrderDto;
import com.coditas.level3.entity.DeliveryMan;
import com.coditas.level3.entity.DeliveryStatus;
import com.coditas.level3.entity.OrderTable;
import com.coditas.level3.entity.Status;
import com.coditas.level3.repository.DeliveryRepository;
import com.coditas.level3.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Order;
import java.time.LocalTime;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class DeliveryManService {

    @Autowired
    DeliveryRepository deliveryRepository;

    @Autowired
    OrderRepository orderRepository;

    public String addDeliveryMan(DeliveryMan deliveryMan)
    {
        DeliveryMan deliveryMan1=new DeliveryMan();
        deliveryMan1.setDeliveryBoyName(deliveryMan.getDeliveryBoyName());
        deliveryMan1.setDeliveryManRating(deliveryMan.getDeliveryManRating());
        deliveryMan1.setX(deliveryMan.getX());
        deliveryMan1.setY(deliveryMan.getY());

        deliveryRepository.save(deliveryMan1);
        return  "DeliveryMan has been added succesfully";
    }

    public String updateOrderByDeliveryMan(UpdateOrderDto updateOrderDto)
    {
        OrderTable orderTable=orderRepository.findById(updateOrderDto.getOrderId()).get();
        orderTable.setOrderStatus(String.valueOf(Status.DELIVERED));
        orderTable.setDeliveredTime(new Date().getTime());

        int actualTimeRequired= (int) (new Date().getTime()-orderTable.getDispatchTime());
        int timeRequired =actualTimeRequired ;
        System.out.println(timeRequired);
        System.out.println(orderTable.getTime()*60*1000);

        if(actualTimeRequired>orderTable.getTime()*60*1000)
        {
            System.out.println("delayy");
            orderTable.setDeliveryStatus(String.valueOf(DeliveryStatus.DELAYED));
        }
        else if(actualTimeRequired<orderTable.getTime()*60*1000)
        {
            System.out.println("early");

            orderTable.setDeliveryStatus(String.valueOf(DeliveryStatus.EARLY));
        }
        else if(actualTimeRequired==orderTable.getTime()*60*1000)
        {
            System.out.println("on time");
            orderTable.setDeliveryStatus(String.valueOf(DeliveryStatus.ONTIME));

        }
        orderRepository.save(orderTable);
        return "Delivery done Successfully .It was "+orderTable.getDeliveryStatus();
    }

    public List<DeliveryManDto> getList()
    {
        List<DeliveryManDto> list=deliveryRepository.findAll().stream().sorted(Comparator.comparing(DeliveryMan::getDeliveryManRating).reversed()).map(r->mapToDto(r)).collect(Collectors.toList());
        return list;
    }

    public DeliveryManDto mapToDto(DeliveryMan deliveryMan)
    {
        DeliveryManDto deliveryManDto=new DeliveryManDto();
        deliveryManDto.setDeliveryBoyId(deliveryMan.getDeliveryBoyId());
        deliveryManDto.setDeliveryBoyName(deliveryMan.getDeliveryBoyName());
        deliveryManDto.setDeliveryManRating(deliveryMan.getDeliveryManRating());

        return deliveryManDto;
    }
}
