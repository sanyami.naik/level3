package com.coditas.level3.controller;


import com.coditas.level3.dto.FoodItemDto;
import com.coditas.level3.entity.Customer;
import com.coditas.level3.entity.FoodItem;
import com.coditas.level3.service.FoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FoodItemController {

    @Autowired
    FoodService foodService;

    @PostMapping("/addFoodItem")
    public String addFoodItem(@RequestBody FoodItemDto foodItemDto)
    {
        return foodService.addFoodItem(foodItemDto);
    }
}
