package com.coditas.level3.controller;


import com.coditas.level3.dto.OrderTableDto;
import com.coditas.level3.entity.Restaurant;
import com.coditas.level3.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {

    @Autowired
    OrderService orderService;

    @PostMapping("/addOrder")
    public String addCustomer(@RequestBody OrderTableDto order)
    {
        return orderService.addOrder(order);
    }
}
