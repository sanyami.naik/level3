package com.coditas.level3.controller;


import com.coditas.level3.dto.RestaurantDto;
import com.coditas.level3.dto.UpdateOrderDto;
import com.coditas.level3.entity.Restaurant;
import com.coditas.level3.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RestaurantController {


    @Autowired
    RestaurantService restaurantService;


    @PostMapping("/addRestaurant")
    public String addCustomer(@RequestBody Restaurant restaurant)
    {
        return restaurantService.addRestaurant(restaurant);
    }

    @GetMapping("/restaurantRatingList")
    public List<RestaurantDto> getList()
    {
        return restaurantService.restaurantRating();
    }

    @PutMapping("/updateOrder")
    public String updateOrder(@RequestBody UpdateOrderDto updateOrderDto)
    {
        return restaurantService.updateOrder(updateOrderDto);
    }
}
