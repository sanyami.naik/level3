package com.coditas.level3.controller;


import com.coditas.level3.entity.Customer;
import com.coditas.level3.service.CustomerService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.Entity;

@RestController
public class CustomerController {


    @Autowired
    CustomerService customerService;

    @PostMapping("/addCustomer")
    public String addCustomer(@RequestBody Customer customer)
    {
       return customerService .addCustomer(customer);
    }

}
