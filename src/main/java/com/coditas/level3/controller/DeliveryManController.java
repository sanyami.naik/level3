package com.coditas.level3.controller;
import com.coditas.level3.dto.DeliveryManDto;
import com.coditas.level3.dto.UpdateOrderDto;
import com.coditas.level3.entity.DeliveryMan;
import com.coditas.level3.service.DeliveryManService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class DeliveryManController {

    @Autowired
    DeliveryManService deliveryManService;



    @PostMapping("/addDeliveryMan")
    public String addDeliveryMan(@RequestBody DeliveryMan deliveryMan)
    {
        return deliveryManService.addDeliveryMan(deliveryMan);
    }

    @GetMapping("/getDeliveryManByRating")
    public List<DeliveryManDto> getList()
    {
        return deliveryManService.getList();
    }

    @PutMapping("/updateOrderByDeliveryMan")
    public String updateOrderByDeliveryMan(@RequestBody UpdateOrderDto updateOrderDto)
    {
        return deliveryManService.updateOrderByDeliveryMan(updateOrderDto);
    }
}
