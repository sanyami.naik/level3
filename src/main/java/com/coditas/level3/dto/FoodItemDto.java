package com.coditas.level3.dto;


import lombok.Data;

@Data
public class FoodItemDto {

    private int foodItemId;
    private String FoodName;
    private float foodPrice;
    private int restaurantId;

}
