package com.coditas.level3.dto;


import lombok.Data;

@Data
public class DeliveryManDto {

    private int deliveryBoyId;
    private String deliveryBoyName;
    private int deliveryManRating;
}
