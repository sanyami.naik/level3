package com.coditas.level3.dto;


import lombok.Data;

@Data
public class RestaurantDto {

    private String restaurantName;
    private int x;
    private int y;
    private int rating;
}
