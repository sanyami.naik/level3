package com.coditas.level3.dto;


import lombok.Data;

import javax.persistence.Entity;
import java.sql.Time;

@Data
public class OrderTableDto {

    private int orderId;
    private int foodItemId;
    private int restaurantId;
    private int deliveryBoyId;
    private int customerId;
    private Time time;

}
