package com.coditas.level3.dto;


import lombok.Data;

@Data
public class UpdateOrderDto {

    private int orderId;
    private String orderStatus;
    private int deliveryBoyId;
    private int customerId;

}
