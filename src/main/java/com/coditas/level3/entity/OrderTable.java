package com.coditas.level3.entity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Entity
public class OrderTable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int orderId;

    @ManyToOne
    private Customer customer;

    @ManyToOne
    private FoodItem foodItem;


    @ManyToOne
    private DeliveryMan deliveryMan;

    @ManyToOne
    private Restaurant restaurant;

    private String orderStatus=Status.RECIEVED.toString();

    private LocalDate date;

    private long dispatchTime;
    private long deliveredTime;

    private String deliveryStatus;

    private int time;


}
