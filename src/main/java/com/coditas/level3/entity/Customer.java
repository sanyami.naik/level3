package com.coditas.level3.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int customerId;

    private String customerName;
    private int x;
    private int y;

    @OneToMany(mappedBy = "customer")
    List<OrderTable> orderList;
}

