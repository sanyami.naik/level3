package com.coditas.level3.entity;

public enum DeliveryStatus {

    ONTIME,

    DELAYED,

    EARLY,
}
