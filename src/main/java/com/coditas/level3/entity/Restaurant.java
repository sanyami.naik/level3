package com.coditas.level3.entity;


import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Restaurant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int restaurantId;
    private String restaurantName;

    private int x;
    private int y;
    private int rating;

    @Override
    public String toString() {
        return "Restaurant{" +
                "restaurantId=" + restaurantId +
                ", restaurantName='" + restaurantName + '\'' +
                ", x=" + x +
                ", y=" + y +
                ", rating=" + rating +
                '}';
    }

    @OneToMany(mappedBy = "restaurant")
    private List<FoodItem> foodItemsList;

    @OneToMany(mappedBy = "restaurant")
    private List<OrderTable> orderTables;

}
