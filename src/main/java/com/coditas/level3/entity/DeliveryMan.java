package com.coditas.level3.entity;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class DeliveryMan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int deliveryBoyId;
    private String deliveryBoyName;

    private int x;
    private int y;

    private int deliveryManRating;


    @OneToMany(mappedBy = "deliveryMan")
    private List<OrderTable> orderList;

    @Override
    public String toString() {
        return "DeliveryMan{" +
                "deliveryBoyId=" + deliveryBoyId +
                ", deliveryBoyName='" + deliveryBoyName + '\'' +
                ", deliveryManRating=" + deliveryManRating +
                '}';
    }
}