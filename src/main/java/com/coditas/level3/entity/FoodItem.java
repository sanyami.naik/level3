package com.coditas.level3.entity;


import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class FoodItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int foodId;
    private String foodName;
    private float foodPrice;
    @ManyToOne
    private Restaurant restaurant;

    @OneToMany(mappedBy = "foodItem")
    private List<OrderTable> orderList;

}
