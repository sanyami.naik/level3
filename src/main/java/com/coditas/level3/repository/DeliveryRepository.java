package com.coditas.level3.repository;

import com.coditas.level3.entity.DeliveryMan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeliveryRepository extends JpaRepository<DeliveryMan,Integer> {
}
