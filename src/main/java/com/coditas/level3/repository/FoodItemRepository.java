package com.coditas.level3.repository;

import com.coditas.level3.entity.FoodItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodItemRepository extends JpaRepository<FoodItem,Integer> {
}
