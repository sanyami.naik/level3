package com.coditas.level3.repository;

import com.coditas.level3.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.criteria.CriteriaBuilder;

public interface CustomerRepository extends JpaRepository<Customer,Integer >{
}
