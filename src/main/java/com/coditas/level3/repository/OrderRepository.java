package com.coditas.level3.repository;

import com.coditas.level3.entity.OrderTable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<OrderTable,Integer> {


}
